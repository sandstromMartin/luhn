# luhn

A command line app that takes an input of numbers (any lenght).

It uses the [Luhn Algorithm](https://en.wikipedia.org/wiki/Luhn_algorithm) to preform a simple checksumon the list digit of the user input. 

For example, providing 4242424242424242:

Input: 424242424242424 2
Provided: 2
Expected: 2

Checksum: Valid
Digits: 16 (credit card) 

Unit test for the code:

Some test by the use of Junit5.