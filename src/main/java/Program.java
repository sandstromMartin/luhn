import java.util.ArrayList;
import java.util.Scanner;

public class Program {


    public static int checkLuhn(String cardNumber)
    {
        int container;
        int evenCounter=0;
        int sum=0;
        int checkdigit;


        ArrayList<Integer> arrayList = new ArrayList<>();
        ArrayList<Integer> newArrayList = new ArrayList<>();

        //loop the value and length of cardNumber and store each char in a int container
        //that will be added to the first arraylist.
        for (int i=0; i<cardNumber.length(); i++){
            container=Character.getNumericValue(cardNumber.charAt(i));
            arrayList.add(container);
        }
        //since we dont want to include the provided character in the calculation,
        //we need to remove the character in the last position of the arraylist.
        arrayList.remove(arrayList.size()-1);


        //the luhn calculation that will loop thrugh the first arraylist.
        //the loop goes backwards
        for(int i=arrayList.size()-1; i>=0 ; i--){
            container=arrayList.get(i);
            evenCounter++;
            //the evenCounter is the one that makes the algorithm ocunt on the numbers at the even position from the input.
            //it let us skit the first, the third and so on, and only count on the numbers at the even position.
            if(!(evenCounter%2==0)){
                //multiplies with 2
                container=container*2;
                //when it is over 10, the value seperates and then adds to a new value ex 12 => 1+2 =3
                if(container>=10){
                    container=(container/10)+(container%10);
                }
                //the new values adds to the new arraylist
            }
            newArrayList.add(container);
        }
        //the values in the new arraylist will be added in the sum as a new int value.
        for (int i=0; i<newArrayList.size();i++){
            sum=sum+newArrayList.get(i);
        }
        //the sum will be multiplied with 9 and modulus 10.
        //the value will then be stored in the checkdigit that will be returned to the main program.
        checkdigit=(sum*9)%10;
        return checkdigit;
    }

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Digits: ");
        String cardNumber = scan.nextLine();

        int provided= 0;
        int checkdigit;

        //store the provided value by take the last character from the input value of cardNumber
        provided = Character.getNumericValue(cardNumber.charAt(cardNumber.length()-1));
        //the length of cardNumber needs to be more than 2 characters. otherwise a message will prints.
        if (cardNumber.length()<2)
        {   System.out.println("To short card number.");
            //if the cardNumber is long enough, the value of cardNumber will be sent to the checkLuhn method.
            //the return value will be stored in checkdigit.
        }else {checkdigit = checkLuhn(cardNumber);
            System.out.println("Input: "+ cardNumber);
            System.out.println("Provided: "+provided);
            System.out.println("Check didgit: "+checkdigit);
            //the value of checkdigit needs to be equals to the value stored in the provided.
            //otherwise the checksum is invalid.
            if (provided==checkdigit){
                System.out.println("Checksum: Valid");
            }else {
                System.out.println("Checksum: Invalid");
            }
            System.out.println("Digits: "+cardNumber.length());
        }


    }
}
