
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class ProgramTest {


    @Test
    public void checkEqual()throws IOException {
        assertEquals(0, Program.checkLuhn("4561237854258962"));
        assertEquals(2, Program.checkLuhn("4242424242424242"));
        assertEquals(6,Program.checkLuhn("1234567"));
    }

    @Test
    public void checkNotEqual(){
        assertNotEquals(2,Program.checkLuhn("3251232584"));
        assertNotEquals(0,Program.checkLuhn("123456789"));
        assertNotEquals(6, Program.checkLuhn("12345"));
    }

    @Test
    public void checkNotNull() {
        assertNotNull(Program.checkLuhn("12345678"));
        assertNotNull(Program.checkLuhn("789456123"));
    }

    @Test
    public void checkNull(){
        assertThrows(NullPointerException.class, () ->{
            Program.checkLuhn(null);
        });
    }

    @Test
    public void checkoutOfBound() {
        assertThrows(IndexOutOfBoundsException.class, () ->{
            Program.checkLuhn("");
        });
    }

}